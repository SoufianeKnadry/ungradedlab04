package geometry;
import org.junit.Test;
import static org.junit.Assert.*;
public class CircleTest {
    
    @Test
    public void testingGetter(){
        Circle circle = new Circle(12);
        assertEquals(12,circle.getCircumference());
    }
    @Test
    public void testArea(){
        Circle circle = new Circle(10);
        assertEquals(7.957747154594767,circle.getArea(),0);
    }
    @Test
    public void testString(){
        Circle circle = new Circle(10);
        assertEquals("The circumference of the circle is : "+circle.getCircumference(),circle.toString());
    }
}
