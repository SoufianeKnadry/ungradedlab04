package geometry;

import org.junit.Test;

import static org.junit.Assert.*;


public class TriangleTest {

    private Triangle triangle = new Triangle(12, 4);

    @Test
    public void testGetter(){
        assertEquals(12, triangle.getBase());
    }

    @Test
    public void testAreaGetter(){
        assertEquals(24, triangle.getArea());
    }

    @Test
    public void testToString(){
        assertEquals("The base is :" + triangle.getBase() + " and the height is: " + triangle.getHeight(), triangle.toString());
    }
}
