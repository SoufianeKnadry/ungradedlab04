
package geometry;

public class Shapes{

    public static void main(String[] args){
        Circle firstCircle = new Circle(10);

        System.out.println(firstCircle+" Area is :"+firstCircle.getArea());
        Triangle triangle = new Triangle(10, 8);
        System.out.println(triangle);
        System.out.println(triangle.getArea());
    }
}