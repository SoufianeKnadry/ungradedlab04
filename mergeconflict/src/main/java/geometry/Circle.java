package geometry;


public class Circle {
    private int circumference;
    
    
    public Circle(int circumference){
        this.circumference = circumference;
    }
    
    public int getCircumference(){
        return this.circumference;
    }

    public double getArea(){
        
        return (this.circumference*this.circumference)/(4*Math.PI);
    }

    @Override
    public String toString(){
        return "The circumference of the circle is : "+this.circumference;
    }
}
