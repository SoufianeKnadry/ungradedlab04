package geometry;

public class Triangle {
    private int base;
    private int height;

    public Triangle(int base, int height){
        this.base = base;
        this.height = height;
    }

    public int getBase(){
        return this.base;
    }

    public int getHeight(){
        return this.height;
    }

    public int getArea(){
        return (base * height) / 2;
    }

    @Override
    public String toString(){
        return "The base is :" + this.base + " and the height is: " + this.height;
    }
}
